<?php

/**
 * Datenbankverbindung
 */
mysql_connect('localhost:3306', 'cruddemo');
mysql_select_db('cruddemo');



$msgs = array();


/**
 * Löschen:
 * - Eintrag aus Datenbank löschen
 * - Bild aus Verzeichnis löschen
 */
if ($_REQUEST['action'] == 'delete') {
	global $msgs;
	
	$result = deleteById($_REQUEST['id']);
	if (is_array($result))
		$msgs = $result;
	else if ($result === true)
		$msgs[] = 'Successfully deleted entry '.$_REQUEST['id'];
	else
		$msgs[] = 'Something went horribly wrong';
}

/**
 * Persistieren
 * - Validieren
 * - ggf. Fehlerbehandlung
 * - Aktualisieren in DB
 */
else if ($_REQUEST['action'] == 'save') {
	$id = $_REQUEST['id'];
	$image = $_REQUEST['image'];
	$title = $_REQUEST['title'];
	$description = $_REQUEST['description'];
	if (array_key_exists('approved', $_REQUEST))
		$approved = 1;
	else
		$approved = 0;
	
	// Speichern, falls keine Probleme aufgetreten sind
	$result = saveOrUpdateData($title, $description, $approved, $image, $id);
	if (is_array($result))
		$msgs = $result;
	else if (is_numeric($result))
		$msgs[] = 'New Entry with id '.$result;
	else if ($result === true)
		$msgs[] = 'changed Entry with id '.$id;
	else
		$msgs[] = 'Something went horribly wrong';
}



/**
 * Upload neuer Bilder
 */
else if ($_REQUEST['action'] == 'upload') {
	$image = $_FILES['image']['name'];
	$title = $_REQUEST['title'];
	$description = $_REQUEST['description'];
	if (array_key_exists('approved', $_REQUEST))
		$approved = 1;
	else
		$approved = 0;
	
	// Validierung des Bildes
	$valResult = validateFileUpload($_FILES['image']);
	if (is_string($valResult)) {
		$msgs[] = $valResult;
	} else {
		// Bewegen des Bildes an die richtige Stelle
		$result = moveUploadedFile($_FILES['image']);
		if (is_array($result)) {
			$msgs = $result;
		} else {
			// Speichern der Bilddaten in die Datenbank
			$result = saveOrUpdateData($title, $description, $approved, $image);
			if (is_array($result))
				$msgs = $result;
			else if (is_numeric($result))
				$msgs[] = 'New Entry with id '.$result;
			else if ($result === true)
				$msgs[] = 'changed Entry with id '.$id;
			else
				$msgs[] = 'Something went horribly wrong';
			}
	}
	
	
	
	
}



function moveUploadedFile($file, $targetDir = '_images/2001/') {
	$tmpFile  = $file['tmp_name'];
	$filename = $file['name'];
	
	if (!move_uploaded_file($tmpFile, $targetDir.$filename)) {
		return array('Datei konnte nicht ins Upload-Verzeichnis verschoben werden');
	}
	
	return true;
}




function validateFileUpload($image) {
	// Fehlerbehandlung
	if ($image['error'] != UPLOAD_ERR_OK) {
		switch($_image['error']) {
			case UPLOAD_ERR_INI_SIZE:
				$msg = 'Datei zu groß laut php.ini';
				break;
				
			case UPLOAD_ERR_FORM_SIZE:
				$msg = 'Datei zu groß laut Formular-Angabe';
				break;
			
			case UPLOAD_ERR_PARTIAL:
				$msg = 'Datei wurde nicht vollständig hoch geladen';
				break;
			
			case UPLOAD_ERR_NO_FILE:
				$msg = 'Es wurde keine Datei hochgeladen.';
				break;
				
			case UPLOAD_ERR_NO_TMP_DIR:
				$msg = 'Auf dem Server wurde kein temporäres Verzeichnis definiert.';
				break;
			
			case UPLOAD_ERR_CANT_WRITE:
				$msg = 'Temporäre Datei konnte nicht geschrieben werden.';
				break;
			
			default:
				$msg = 'Some unknown error occured. I\'m very sorry.';
		}	
		return array($msg);
	}
	
	return true;
}




function getData() {
	$sql = "SELECT id, image, title, description, approved FROM images";
	$result = mysql_query($sql);
	$return = array();
	while ($entry = mysql_fetch_assoc($result)) {
		$return[] = array (
			'id' => $entry['id'],
			'image' => $entry['image'],
			'title' => $entry['title'],
			'description' => $entry['description'],
			'approved' => $entry['approved']);
	}
	return $return;
}



function deleteById($id = null) 
{
	if ($id === null || !is_numeric($id))
		return array('ID can\'t be null or non-numeric');
	
	
	// Bild löschen
	$sql = 'SELECT image FROM images WHERE id='.mysql_real_escape_string($id);
	$result = mysql_query($sql);
	if (mysql_num_rows($result) != 1)
		return array('Could not find ID '.$id.' in database');
	
	list($image) = mysql_fetch_array($result);
	if (!file_exists('_images/'.$image))
		return array('Could not find image for ID '.$id.' - should be '.$image);
	
	if (!unlink('_images/'.$image))
		return array('Could not delete picture of ID '.$id.', which should be _images/'.$image);
	
	
	// Eintrag aus Datenbank löschen
	$sql = 'DELETE FROM images WHERE id='.mysql_real_escape_string($id);
	mysql_query($sql);
	$affRows = mysql_affected_rows();
	if ($affRows == 0)
		return array('Could not delete - ID '.$id.' not found');
	if ($affRows == 1)
		return true;
	
	return array('Something horrible has happened - we deleted '.$affRows.' images from database');
}


function saveOrUpdateData($title, $description, $approved, $image, $id=null)
{
	$errors = validateData($title, $description, $approved, $image, $id);
	if (!empty($errors))
		return $errors;
	
	global $data;
	// falls neuer Eintrag angelegt werden soll
	if ($id === null) {
		$sql = 'INSERT INTO images (title, image, description, approved)';
		$sql .= " VALUES ('".mysql_real_escape_string($title)."',";
		$sql .= "'".mysql_real_escape_string('2001/'.$image)."',";
		$sql .= "'".mysql_real_escape_string($description)."',";
		$sql .= "".mysql_real_escape_string($approved).")";
		
		$result = mysql_query($sql);
		
		if (mysql_affected_rows() == 0)
			return array('Could not save data - error was '.mysql_error());
		if (mysql_affected_rows() == 1)
			return mysql_insert_id();
		
		return array('Something horrible happened - error was '.mysql_error());
	} 
	
	// falls existierender Eintrag geändert werden soll
	else {
		$sql = 'UPDATE images SET ';
		$sql .= "title='".mysql_real_escape_string($title)."',";
		$sql .= "image='".mysql_real_escape_string($image)."',";
		$sql .= "description='".mysql_real_escape_string($description)."',";
		$sql .= "approved=".mysql_real_escape_string($approved)." ";
		$sql .= "WHERE id=".mysql_real_escape_string($id);
		
		$result = mysql_query($sql);
		
		if (mysql_affected_rows() == 0 && mysql_errno() != 0)
			return array('Could not save data - error was '.mysql_error());
		if (mysql_affected_rows() == 1 || mysql_errno() == 0) {
			unset($_REQUEST['title']);
			unset($_REQUEST['description']);
			unset($_REQUEST['approved']);
			return true;
		}
		
		return array('Something horrible happened - error was '.mysql_error());
	}
}


function validateData($title, $description, $approved, $image, $id=null)
{
	$errors = array();
	
	if ($id !== null && !is_numeric($id))
		$errors[] = 'id has to be numeric';
	if (!preg_match('/^[a-zA-Z0-9-_ ]{3,32}$/', $title))
		$errors[] = 'Title has to be alphanumeric and between 3 and 32 characters';
	if (!preg_match('/^.{3,256}$/', $description))
		$errors[] = 'Description has to be alphanumeric and between 3 and 256 characters';
	if (!preg_match('/^[01]$/', $approved))
		$errors[] = 'Approved has to be zero or one';
	if (!preg_match('/^[a-zA-Z0-9\/ ]{3,32}\.(jpg|jpeg|png)$/', $image))
		$errors[] = 'ImageName must be alphanumeric without spaces and end on jpg|jpeg|png - got: '.$image;
	
	return $errors;
}

// get data
$data = getData();

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Foob</title>
	</head>
	<body>
		<?php if (!empty($msgs)) : ?>
		<ul>
		<?php foreach($msgs as $msg) : ?>
			<li><?=$msg?></li>
		<?php endforeach; ?>
		</ul>
		<?php endif; ?>
		
		<?php if (empty($data)) : ?>
		<p>
			No Pictures in database!
		</p>
		<?php else : ?>
		<table>
			<tr>
				<th>Thumb</th>
				<th>Title</th>
				<th>Desc.</th>
				<th>Appr.?</th>
				<th>Operations</th>
			</tr>
			<tr style="background-color:#960;">
				<form action="" enctype="multipart/form-data" method="POST">
				<td>
					<input type="file" name="image">
				</td>
				<td>
					<input type="text" name="title" value="<?= ($_REQUEST['action'] == 'upload') ? $_REQUEST['title'] : '' ?>">
				</td>
				<td>
					<textarea name="description"><?= ($_REQUEST['action'] == 'upload') ? $_REQUEST['description'] : '' ?></textarea>
				</td>
				<td>
					<input type="checkbox" name="approved" <?php echo ($_REQUEST['action'] == 'upload' && $_REQUEST['approved']) ? 'checked' : ''?>>
				</td>
				<td>
					<input type="submit" name="action" value="upload">
				</td>
				</form>
			</tr>
			<?php foreach ($data as $entry) : ?>
			<tr>
				<form action="" method="post">
				<td>
					<input type="hidden" name="id" value="<?=$entry['id']?>">
					<img src="_images/<?=$entry['image']?>" style="height:64px;">
					<input type="hidden" name="image" value="<?=$entry['image']?>">
				</td>
				<td>
					<input type="text" name="title" value="<?=$entry['title']?>">
				</td>
				<td>
					<textarea name="description"><?=$entry['description']?></textarea>
				</td>
				<td>
					<input type="checkbox" name="approved" <?php echo ($entry['approved']) ? 'checked' : '' ?>>
				</td>
				<td>
					<input type="submit" name="action" value="delete">
					<input type="submit" name="action" value="save">
				</td>
				</form>
			</tr>
			<?php endforeach; ?>
		</table>
		<?php endif; ?>
	</body>
</html>
